class Bomb {
    constructor(element, x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.element = element;
        this.bombs = 0;
        this.count = 4;
    }

    set element(mesh) {
        if (mesh instanceof THREE.Mesh) {
            this._element = mesh;
        } else {
            let geometry = new THREE.SphereGeometry(1, 20, 20)
            let material = new THREE.MeshPhongMaterial({ color: 0x000000, wireframe: false });
            this._element = new THREE.Mesh(geometry, material);
            this._element.castShadow = true;
            this._element.receiveShadow = true;
            this._element.position.set(this.x, this.y, this.z)
        }
    }

    get element() {
        return this._element;
    }

    set Count(value) {
        this.count += value;
    }

    get Count() {
        return this.count;
    }

    set Bombs(value) {
        this.bombs = value;
    }

    get Bombs() {
        return this.bombs;
    }

    play(scene, control) {
        this.bombs = control.bombs;
        if (this.bombs == 1) {
            scene.add(this.element);
            this.counter = setInterval(() => {
                timer(this,control);
              }, 1000);
        }
    }

}

function timer(bomb, control) {
    if (bomb.count == 0) {
        clearInterval(bomb.counter);
        control.bombs = 0;
        console.log("exploto");
        scene.remove(bomb.element);
    }
    bomb.count--;
    console.log(Control.bombs);
}