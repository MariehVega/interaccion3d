class Player {

    constructor(name, element, control, ap = {}) {
        this.name = name;
        this.control = control;
        this.element = element;
        this.label = this.getLabel();

        if ("label" in ap) {
            if (ap.label) {
                this.showLabel();
            }
        }
    }

    set element(mesh) {
        if (mesh instanceof THREE.Mesh) {
            this._element = mesh;
            var helper = new THREE.BoundingBoxHelper(this._element, 0xffffff);
            helper.update();
            this._element.add(helper);

        } else {
            let geometry = new THREE.SphereGeometry(60, 20, 20);
            let material = new THREE.MeshPhongMaterial({
                color: 0x2ECCFA,
                shininess: 2,
                specular: 0xffffff,
                shading: THREE.FlatShading
            });
            this._element = new THREE.Mesh(geometry, material);
            var helper = new THREE.BoundingBoxHelper(this._element, 0xffffff);
            helper.update();
            this._element.add(helper);
        }
        this.control.element = this._element;
    }

    get element() {
        return this._element;
    }

    updateControls() {
        this.control.update();
    }

    getLabel() {
        return Utilities.label(
            this.element.position,
            Utilities.textTure(this.name, 128, "Bold", "10px", "Arial", "0,0,0,1", 64, 50)
        )
    }

    showLabel() {
        this.element.add(this.label);
    }

    play(scene) {
        this.collidableBox = new CollidableBox(this._element, 1);
        //this.element.position.y = -2460;
        scene.add(this.element);
    }
}