//Ya que todos van a tener estos valores

var vx = 0.5;
var vy = 0;
var fg = 0.5;
var groundPosition = 0.5;
class Control {
    constructor(up, right, down, left, jump, place) {
        this.initControls();
        this.up = up || "w";
        this.right = right || "d";
        this.down = down || "s";
        this.left = left || "a";
        this.jump = jump || " ";
        this.place = place || "x";
        this.vx = vx;
        this.vy = vy;

        this.onGround = false;
        this.element = null;
        this.bombs = 0;

        this.initListeners();
    }

    set up(key) {
        this._up.key = key;
    }

    get up() {
        return this._up.key;
    }

    set right(key) {
        this._right.key = key;
    }

    get right() {
        return this._right.key;
    }

    set down(key) {
        this._down.key = key;
    }

    get down() {
        return this._down.key;
    }

    set left(key) {
        this._left.key = key;
    }

    get left() {
        return this._left.key;
    }

    set jump(key) {
        this._jump.key = key;
    }

    get jump() {
        return this._jump.key;
    }

    set place(key) {
        this._place.key = key;
    }

    get place() {
        return this._place.key;
    }

    initControls() {
        this._up = { key: "", isPressed: false };
        this._right = { key: "", isPressed: false };
        this._down = { key: "", isPressed: false };
        this._left = { key: "", isPressed: false };
        this._jump = { key: "", isPressed: false };
        this._place = { key: "", isPressed: false };
    }

    initListeners() {


    }

    update() {
        this.vy -= fg;
        this.element.position.y += this.vy;
        if (this.element.position.y < groundPosition) {
            this.element.position.y = groundPosition;
            this.vy = vy;
            this.onGround = true;
        }

        if (this._up.isPressed) {
            this.element.position.z -= this.vx;
        }
        if (this._right.isPressed) {
            this.element.position.x += this.vx;
        }
        if (this._down.isPressed) {
            this.element.position.z += this.vx;
        }
        if (this._left.isPressed) {
            this.element.position.x -= this.vx;
        }
    }

    pressUp() {
        this._up.isPressed = true;
    }
    pressRight() {
        this._right.isPressed = true;
    }
    pressDown() {
        this._down.isPressed = true;
    }
    pressLeft() {
        this._left.isPressed = true;
    }
    pressJump() {
        this._jump.isPressed = true;
        if (this.onGround) {
            this.vy = 2.0;
            this.onGround = false;
        }
    }
    pressPlace() {
        this.bombs++;
        this._place.isPressed = true;
            this.bombita = new Bomb(null, this.element.position.x, this.element.position.y, this.element.position.z);
            this.bombita.play(scene, this);


    }

    releaseUp() {
        this._up.isPressed = false;
    }
    releaseRight() {
        this._right.isPressed = false;
    }
    releaseDown() {
        this._down.isPressed = false;
    }
    releaseLeft() {
        this._left.isPressed = false;
    }
    releaseJump() {
        this._jump.isPressed = false;
        if (this.vy > 1.0) {
            this.vy = 1.0;
        }
    }
    releasePlace() {
        this._place.isPressed = false;
    }

}


document.onkeydown = (e) => {

    for (let i = 0; i < Object.keys(players).length; i++) {
        let key = Object.keys(players)[i];
        if (players[key] == null) { return false; }
        let elControl = players[key]["control"];
        switch (e.key) {
            case elControl.up:
                elControl.pressUp();
                break;
            case elControl.right:
                elControl.pressRight();
                break;
            case elControl.down:
                elControl.pressDown();
                break;
            case elControl.left:
                elControl.pressLeft();
                break;
            case elControl.jump:
                elControl.pressJump();
                break
            case elControl.place:
                elControl.pressPlace();
                break
            case "1":
                cameras.current = cameras.tpersona;
                break;
            case "2":
                cameras.current = cameras.default;
                break;
        }

    }

}

document.onkeyup = (e) => {

    for (let i = 0; i < Object.keys(players).length; i++) {

        let key = Object.keys(players)[i];
        if (players[key] == null) { return false; }
        let elControl = players[key]["control"];

        switch (e.key) {
            case elControl.up:
                elControl.releaseUp();
                break;
            case elControl.right:
                elControl.releaseRight();
                break;
            case elControl.down:
                elControl.releaseDown();
                break;
            case elControl.left:
                elControl.releaseLeft();
                break;
            case elControl.jump:
                elControl.releaseJump();
                break;
            case elControl.place:
                elControl.releasePlace();
                break;
        }
    }
}