/**
 * GLOBAL VARS
 */
cameras = {
    default: null,
    current: null
};
canvas = {
    element: null,
    container: null
}
labels = {}
cameraControl = null;
scene = null;
renderer = null

players = {
    p1: null,
    p2: null,
    p3: null,
    p4: null
}
collidableList = [];

/**
 * Function to start program running a
 * WebGL Application trouhg ThreeJS
 */
let webGLStart = () => {
    initScene();
    window.onresize = onWindowResize;
    lastTime = Date.now();
    animateScene();
};

/**
 * Here we can setup all our scene noobsters
 */
function initScene() {
    //Selecting DOM Elements, the canvas and the parent element.
    canvas.container = document.querySelector("#app");
    canvas.element = canvas.container.querySelector("#appCanvas");

    /**
     * SETTING UP CORE THREEJS APP ELEMENTS (Scene, Cameras, Renderer)
     * */
    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer({ canvas: canvas.element });
    renderer.setSize(canvas.container.clientWidth, canvas.container.clientHeight);
    renderer.setClearColor(0x20273a, 1);

    canvas.container.appendChild(renderer.domElement);

    //positioning cameras
    cameras.default = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 0.1, 1000);
    cameras.default.position.set(0, 30, 60);
    cameras.default.lookAt(new THREE.Vector3(0, 0, 0));

    //CAMERAS
    var tracking = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 1, 10000);
	tracking.position.set(1600,1600,-1600);
	tracking.lookAt(new THREE.Vector3(0,0,0));

	var fixed = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 1, 10000);
	fixed.position.set(-600,300,-600);
	fixed.lookAt(new THREE.Vector3(0,0,0));

	var tpersona = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 1, 10000);
	tpersona.position.set(1600,1600,-1600);
	tpersona.lookAt(new THREE.Vector3(0,0,0));

	var observer = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 1, 10000);
	observer.position.set(1600,1600,-1600);
	observer.lookAt(new THREE.Vector3(0,0,0));

	cameras.tracking = tracking;
	cameras.fixed = fixed;
	cameras.tpersona = tpersona;
    cameras.observer = observer;
    //Setting up current default camera as current camera
    cameras.current = cameras.default;
    
    //Camera control Plugin
    cameraControl = new THREE.OrbitControls(cameras.current, renderer.domElement);


    lAmbiente = new THREE.AmbientLight(0xffffff);
    scene.add(lAmbiente);

    //FPS monitor
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = stats.domElement.style.left = '10px';
    stats.domElement.style.zIndex = '100';
    document.body.appendChild(stats.domElement);

    //lights

    var spotLight = new THREE.SpotLight(0xffffff, 1, 0);
    spotLight.position.set(0, 50, 0);
    spotLight.angle = 0.0872665;
    spotLight.penumbra = 0.5;
    spotLight.castShadow = true;

    //Init player with controls
    let esfera = new THREE.SphereGeometry(1, 20, 20);
    let material1 = new THREE.MeshPhongMaterial({
        color: 0x2ECCFA,
        shininess: 2,
        specular: 0xffffff,
        shading: THREE.FlatShading
    });
    var player1 = new THREE.Mesh(esfera, material1);
    player1.castShadow = true;
    players.p1 = new Player("P1", player1, new Control(), { label: false });
    players.p1.play(scene);
    spotLight.target = players.p1.element;

    //Init player with controls
    let materiales = new THREE.MeshPhongMaterial({
        color: 0xff1102,
        shininess: 2,
        specular: 0xffffff,
        shading: THREE.FlatShading
    });
    var player2 = new THREE.Mesh(esfera, materiales);
    player2.castShadow = true;
    players.p2 = new Player("P2", player2, new Control("i","l","k","j","m","n"), { label: false });
    players.p2.play(scene);

    //Generamos el escenario

    colisionable = new THREE.Mesh(
        new THREE.BoxGeometry(2,2,2),
        new THREE.MeshBasicMaterial( {color:0x6a6a6a})
    );
    colisionable.position.set(-15,0,0);
    scene.add(colisionable);

    var plano1 = new THREE.PlaneGeometry(50, 50, 11, 10);
    var material2 = new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture('assets/suelo2.jpg')
    });
    var suelo = new THREE.Mesh(plano1, material2);
    suelo.receiveShadow = true;
    suelo.rotation.x += -1.5708;
    suelo.position.y += -1;

    var caja1 = new THREE.BoxGeometry(15, 7, 1, 10, 10, 10);
    var material3 = new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture('assets/suelo3.jpg')
    });
    var pared1 = new THREE.Mesh(caja1, material3);
    pared1.receiveShadow = true;
    pared1.position.x = 17.5
    pared1.position.y = 2.5
    pared1.position.z = 24.5

    var caja2 = new THREE.BoxGeometry(15, 7, 1, 10, 10, 10);
    var pared2 = new THREE.Mesh(caja2, material3);
    pared2.receiveShadow = true;
    pared2.position.x = -17.5
    pared2.position.y = 2.5
    pared2.position.z = -24.5

    var caja3 = new THREE.BoxGeometry(1, 7, 15, 10, 10, 10);
    var pared3 = new THREE.Mesh(caja3, material3);
    pared3.receiveShadow = true;
    pared3.position.x = -24.5
    pared3.position.y = 2.5
    pared3.position.z = -16.5

    var caja4 = new THREE.BoxGeometry(1, 7, 15, 10, 10, 10);
    var pared4 = new THREE.Mesh(caja4, material3);
    pared4.receiveShadow = true;
    pared4.position.x = 24.5
    pared4.position.y = 2.5
    pared4.position.z = 16.5

    uvSuelo = [
        new THREE.Vector2(0, 1),
        new THREE.Vector2(0, 0),
        new THREE.Vector2(1, 0),
        new THREE.Vector2(1, 1)
    ];

    suelo.geometry.faceVertexUvs[0] = [];

    for (let i = 0, j = 1, k = 0; k < 220; i = i + 2, j = j + 2, k++) {
        if (k % 2 == 0) {
            suelo.geometry.faceVertexUvs[0][i] = [
                uvSuelo[0],
                uvSuelo[1],
                uvSuelo[3]
            ];
            suelo.geometry.faceVertexUvs[0][j] = [
                uvSuelo[1],
                uvSuelo[2],
                uvSuelo[3]
            ];
        } else {
            suelo.geometry.faceVertexUvs[0][i] = [
                uvSuelo[3],
                uvSuelo[0],
                uvSuelo[2]
            ];
            suelo.geometry.faceVertexUvs[0][j] = [
                uvSuelo[0],
                uvSuelo[1],
                uvSuelo[2]
            ];
        }

    }

    scene.add(suelo, spotLight, pared1, pared2, pared3, pared4);

    collidableList.push(pared1);
    collidableList.push(pared2);
    collidableList.push(pared3);
    collidableList.push(pared4);
    collidableList.push(colisionable);
    collidableList.push(suelo);

    initObjects();
}

/**
 * Function to add all objects, lights (except for the ambienlight) and stuff to scene
 */
function initObjects() {

    sonidoIzquierda = new Sound(["assets/1.mp3"], 20, scene, {
        debug: true,
        position: { x: 23, y: 0, z: 23 }
    });

    sonidoDerecha = new Sound(["assets/2.mp3"], 20, scene, {
        debug: true,
        position: { x: -23, y: 0, z: -23 }
    });

}

/**
 * Function to render application over
 * and over.
 */
function animateScene() {
    requestAnimationFrame(animateScene);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.render(scene, cameras.current);
    updateScene();
}

/**
 * Function to evaluate logic over and
 * over again.
 */
function updateScene() {

    //Updating camera view by control inputs
    cameraControl.update();
    //Updating FPS monitor
    stats.update();
    //Sound Update
    sonidoIzquierda.update(players.p1.element);
    sonidoDerecha.update(players.p1.element);

    //Players controls
    for (const player of Object.keys(players)) {
        if( players[player] != null ){
            players[player].updateControls();
            players[player].collidableBox.update(players[player].control);
        }
    }

    for (const label of Object.keys(labels)) {
        labels[label].lookAt(cameras.current.position);
        if (label == "p1") {
            labels[label].position.copy(players.p1.element.position);
        }
        if (label == "p2") {
            labels[label].position.copy(players.p2.element.position);
        }
    }

    //cameras
    cameras.tpersona.position.copy(players.p1.element.position);
        cameras.tpersona.position.y += 300;
        cameras.tpersona.position.x += 300;
        cameras.tpersona.position.z -= 300;

}

function onWindowResize() {
    cameras.current.aspect = window.innerWidth / window.innerHeight;
    cameras.current.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}